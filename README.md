[travis-badge]: https://travis-ci.org/hpedrorodrigues/Hyperion.svg?branch=master
[travis-repo]: https://travis-ci.org/hpedrorodrigues/Hyperion
[codecov-badge]: https://codecov.io/gh/hpedrorodrigues/Hyperion/branch/master/graph/badge.svg
[codecov-repo]: https://codecov.io/gh/hpedrorodrigues/Hyperion
[jitpack-badge]: https://jitpack.io/v/hpedrorodrigues/Hyperion.svg
[jitpack-url]: https://jitpack.io/#hpedrorodrigues/Hyperion

[![Build Status][travis-badge]][travis-repo]
[![Codecov][codecov-badge]][codecov-repo]
[![Jitpack][jitpack-badge]][jitpack-url]

# DEPRECATED
This project is not maintained anymore.

# Hyperion

Generate plain language entities from JSON.

**Supported languages**:

- Java
- Scala
- Kotlin
- C#
- PHP

## Installation

**Step 1** - Add the JitPack repository.

```xml
<repositories>
    <repository>
        <id>jitpack.io</id>
        <url>https://jitpack.io</url>
    </repository>
</repositories>
```

**Step 2** - Add the dependency.

```xml
    <dependency>
        <groupId>com.github.hpedrorodrigues</groupId>
        <artifactId>Hyperion</artifactId>
        <version>1.1.0-SNAPSHOT</version>
    </dependency>
```

## Usage

```java
import com.hpedrorodrigues.hyperion.configuration.Metadata;

final Metadata metadata = new Metadata();

metadata.setNamespace("com.example");
metadata.setRootEntityName("Example");
metadata.setLanguage(Language.JAVA);
metadata.setJson("{\"a\": \"a\"}");

Hyperion.parse(metadata);

// Or just
Hyperion.parse(Language.JAVA, "{\"a\": \"a\"}");
```

**Output** for this example:
```java
package com.example;

public class Example {
 
   private String a;

   public String getA() {
     return this.a;
   }
 
   public void setA(String a) {
     this.a = a;
   }
}
```

## License

    Copyright 2016 Pedro Rodrigues

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
