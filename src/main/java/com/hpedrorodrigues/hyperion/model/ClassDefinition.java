package com.hpedrorodrigues.hyperion.model;

import com.hpedrorodrigues.hyperion.util.CollectionUtil;
import lombok.Data;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Data
public class ClassDefinition {

    private String fileName;

    private String namespace;

    private String start;

    private String end;

    private List<Field> fields;

    private Collection<JSONField> jsonFields;

    public void addField(final Field field) {
        if (CollectionUtil.isEmpty(fields)) {
            fields = new ArrayList<>();
        }

        fields.add(field);
    }

    public void addFields(final Collection<JSONField> fields) {
        if (CollectionUtil.isEmpty(this.jsonFields)) {
            this.jsonFields = new ArrayList<>();
        }

        this.jsonFields.addAll(fields);
    }
}