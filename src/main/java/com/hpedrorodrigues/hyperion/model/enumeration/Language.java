package com.hpedrorodrigues.hyperion.model.enumeration;

public enum Language {

    JAVA, SCALA, KOTLIN, CSHARP, PHP
}