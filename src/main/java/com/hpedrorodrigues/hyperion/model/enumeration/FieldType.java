package com.hpedrorodrigues.hyperion.model.enumeration;

public enum FieldType {

    FIELD, CLASS
}