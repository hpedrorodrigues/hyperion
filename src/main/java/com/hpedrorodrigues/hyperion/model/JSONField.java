package com.hpedrorodrigues.hyperion.model;

import com.hpedrorodrigues.hyperion.model.enumeration.FieldType;
import lombok.Data;

@Data
public class JSONField {

    private FieldType type;

    private Field field;

    private ClassDefinition clazz;
}