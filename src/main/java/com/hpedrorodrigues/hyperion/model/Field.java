package com.hpedrorodrigues.hyperion.model;

import lombok.Data;

@Data
public class Field {

    private String definition;

    private String getter;

    private String setter;
}