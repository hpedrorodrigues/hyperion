package com.hpedrorodrigues.hyperion.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class PlainEntity {

    private String name;
    private String content;
}