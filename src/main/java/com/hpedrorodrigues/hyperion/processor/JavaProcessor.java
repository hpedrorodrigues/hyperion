package com.hpedrorodrigues.hyperion.processor;

import com.hpedrorodrigues.hyperion.definition.JavaDefinition;
import com.hpedrorodrigues.hyperion.processor.base.AbstractProcessor;
import com.hpedrorodrigues.hyperion.type.JavaType;

public class JavaProcessor extends AbstractProcessor<JavaType, JavaDefinition> {

    public JavaProcessor() {
        super(new JavaType(), new JavaDefinition());
    }

    @Override
    public String getNamespace(String key) {
        return new StringBuilder()
                .append("package ")
                .append(key)
                .append(definition.getEndLine())
                .append(definition.getBreakLine())
                .append(definition.getBreakLine())
                .toString();
    }

    @Override
    public String getFileName(final String key) {
        return capitalize(key) + "." + definition.getExtension();
    }

    @Override
    public String getStartClass(final String key) {
        return new StringBuilder()
                .append("public class ")
                .append(capitalize(key))
                .append(" {")
                .append(definition.getBreakLine())
                .toString();
    }

    @Override
    public String getType(final String key, final String typeName) {
        return new StringBuilder()
                .append(definition.getBreakLine())
                .append(definition.getTab())
                .append("private ")
                .append(typeName)
                .append(" ")
                .append(decapitalize(key))
                .append(definition.getEndLine())
                .toString();
    }

    @Override
    public String getListType(final String key, final String typeName) {
        return getType(key, getListTypeName(typeName));
    }

    @Override
    public String getListTypeName(final String typeName) {
        return "List<" + typeName + ">";
    }

    @Override
    public String getListSetter(final String key, final String typeName) {
        return getSetter(key, getListTypeName(typeName));
    }

    @Override
    public String getListGetter(final String key, final String typeName) {
        return getGetter(key, getListTypeName(typeName));
    }

    @Override
    public String getGetter(final String key, final String typeName) {
        final String capitalized = capitalize(key);
        final String decapitalized = decapitalize(key);

        return new StringBuilder()
                .append(definition.getBreakLine())
                .append(definition.getTab())
                .append("public ")
                .append(typeName)
                .append(" get")
                .append(capitalized)
                .append("() {")
                .append(definition.getBreakLine())
                .append(definition.getTab())
                .append(definition.getTab())
                .append("return ")
                .append(definition.getSelfReference())
                .append(definition.getDelimiter())
                .append(decapitalized)
                .append(definition.getEndLine())
                .append(definition.getBreakLine())
                .append(definition.getTab())
                .append("}")
                .append(definition.getBreakLine())
                .toString();
    }

    @Override
    public String getSetter(final String key, final String typeName) {
        final String capitalized = capitalize(key);
        final String decapitalized = decapitalize(key);

        return new StringBuilder()
                .append(definition.getBreakLine())
                .append(definition.getTab())
                .append("public void set")
                .append(capitalized)
                .append("(")
                .append(typeName)
                .append(" ")
                .append(decapitalized)
                .append(") {")
                .append(definition.getBreakLine())
                .append(definition.getTab())
                .append(definition.getTab())
                .append(definition.getSelfReference())
                .append(definition.getDelimiter())
                .append(decapitalized)
                .append(" = ")
                .append(decapitalized)
                .append(definition.getEndLine())
                .append(definition.getBreakLine())
                .append(definition.getTab())
                .append("}")
                .append(definition.getBreakLine())
                .toString();
    }
}