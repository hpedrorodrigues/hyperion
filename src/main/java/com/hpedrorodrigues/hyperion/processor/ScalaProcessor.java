package com.hpedrorodrigues.hyperion.processor;

import com.hpedrorodrigues.hyperion.definition.ScalaDefinition;
import com.hpedrorodrigues.hyperion.processor.base.AbstractProcessor;
import com.hpedrorodrigues.hyperion.type.ScalaType;

public class ScalaProcessor extends AbstractProcessor<ScalaType, ScalaDefinition> {

    public ScalaProcessor() {
        super(new ScalaType(), new ScalaDefinition());
    }

    @Override
    public String getNamespace(String key) {
        return new StringBuilder()
                .append("package ")
                .append(key)
                .append(definition.getBreakLine())
                .append(definition.getBreakLine())
                .toString();
    }

    @Override
    public String getFileName(final String key) {
        return capitalize(key) + "." + definition.getExtension();
    }

    @Override
    public String getStartClass(final String key) {
        return new StringBuilder()
                .append("public class ")
                .append(capitalize(key))
                .append(" {")
                .append(definition.getBreakLine())
                .append(definition.getBreakLine())
                .toString();
    }

    @Override
    public String getType(final String key, final String typeName) {
        return new StringBuilder()
                .append(definition.getTab())
                .append("private var _")
                .append(decapitalize(key))
                .append(": ")
                .append(typeName)
                .append(" = ")
                .append(getDefaultValueByTypeName(typeName))
                .append(definition.getBreakLine())
                .toString();
    }

    @Override
    public String getGetter(final String key, final String typeName) {
        final String decapitalized = decapitalize(key);

        return new StringBuilder()
                .append(definition.getTab())
                .append("def ")
                .append(decapitalized)
                .append(" = _")
                .append(decapitalized)
                .append(definition.getBreakLine())
                .toString();
    }

    @Override
    public String getSetter(final String key, final String typeName) {
        final String decapitalized = decapitalize(key);

        return new StringBuilder()
                .append(definition.getTab())
                .append("def ")
                .append(decapitalized)
                .append("_= (value: ")
                .append(typeName)
                .append("): Unit = _")
                .append(decapitalized)
                .append(" = ")
                .append("value")
                .append(definition.getBreakLine())
                .append(definition.getBreakLine())
                .toString();
    }

    @Override
    public String getListType(final String key, final String typeName) {
        return getType(key, getListTypeName(typeName));
    }

    @Override
    public String getListTypeName(final String typeName) {
        return "List[" + typeName + "]";
    }

    @Override
    public String getListSetter(final String key, final String typeName) {
        return getSetter(key, getListTypeName(typeName));
    }

    @Override
    public String getListGetter(final String key, final String typeName) {
        return getGetter(key, getListTypeName(typeName));
    }
}