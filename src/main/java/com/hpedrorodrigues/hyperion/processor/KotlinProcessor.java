package com.hpedrorodrigues.hyperion.processor;

import com.hpedrorodrigues.hyperion.definition.KotlinDefinition;
import com.hpedrorodrigues.hyperion.processor.base.AbstractProcessor;
import com.hpedrorodrigues.hyperion.type.KotlinType;

public class KotlinProcessor extends AbstractProcessor<KotlinType, KotlinDefinition> {

    public KotlinProcessor() {
        super(new KotlinType(), new KotlinDefinition());
    }

    @Override
    public String getNamespace(String key) {
        return new StringBuilder()
                .append("package ")
                .append(key)
                .append(definition.getBreakLine())
                .append(definition.getBreakLine())
                .toString();
    }

    @Override
    public String getFileName(final String key) {
        return capitalize(key) + "." + definition.getExtension();
    }

    @Override
    public String getStartClass(final String key) {
        return new StringBuilder()
                .append("class ")
                .append(capitalize(key))
                .append(" {")
                .toString();
    }

    @Override
    public String getType(final String key, final String typeName) {
        return new StringBuilder()
                .append(definition.getBreakLine())
                .append(definition.getBreakLine())
                .append(definition.getTab())
                .append("var ")
                .append(decapitalize(key))
                .append(": ")
                .append(typeName)
                .append(" = ")
                .append(getDefaultValueByTypeName(typeName))
                .toString();
    }

    @Override
    public String getGetter(final String key, final String typeName) {
        return "";
    }

    @Override
    public String getSetter(final String key, final String typeName) {
        return "";
    }

    @Override
    public String getListType(final String key, final String typeName) {
        return getType(key, getListTypeName(typeName));
    }

    @Override
    public String getListTypeName(final String typeName) {
        return "List<" + typeName + ">";
    }

    @Override
    public String getListSetter(final String key, final String typeName) {
        return "";
    }

    @Override
    public String getListGetter(final String key, final String typeName) {
        return "";
    }
}
