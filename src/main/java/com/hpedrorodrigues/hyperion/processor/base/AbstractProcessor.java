package com.hpedrorodrigues.hyperion.processor.base;

import com.hpedrorodrigues.hyperion.definition.AbstractDefinition;
import com.hpedrorodrigues.hyperion.type.base.AbstractType;
import com.hpedrorodrigues.hyperion.util.StringUtil;

public abstract class AbstractProcessor<T extends AbstractType, D extends AbstractDefinition> {

    protected final T type;
    protected final D definition;

    public AbstractProcessor(final T type, final D definition) {
        this.type = type;
        this.definition = definition;
    }

    public T getType() {
        return type;
    }

    public D getDefinition() {
        return definition;
    }

    public abstract String getNamespace(final String key);

    public abstract String getFileName(final String key);

    public abstract String getStartClass(final String key);

    public abstract String getType(final String key, final String typeName);

    public abstract String getGetter(final String key, final String typeName);

    public abstract String getSetter(final String key, final String typeName);

    public abstract String getListType(final String key, final String typeName);

    public abstract String getListTypeName(final String typeName);

    public abstract String getListSetter(final String key, final String typeName);

    public abstract String getListGetter(final String key, final String typeName);

    public String capitalize(final String value) {
        return StringUtil.capitalize(value);
    }

    public String decapitalize(final String value) {
        return StringUtil.decapitalize(value);
    }

    public String getDefaultValueByTypeName(final String typeName) {
        if (typeName.equals(type.getText())) {
            return type.getDefaultTextValue();
        } else if (typeName.equals(type.getInteger())) {
            return type.getDefaultIntegerValue();
        } else if (typeName.equals(type.getFloating())) {
            return type.getDefaultFloatingValue();
        } else if (typeName.equals(type.getBoolean())) {
            return type.getDefaultBooleanValue();
        } else if (typeName.toLowerCase().contains("list")) {
            return type.getDefaultNullListValue();
        } else {
            return type.getDefaultNullValue();
        }
    }
}