package com.hpedrorodrigues.hyperion.parser.base;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.hpedrorodrigues.hyperion.configuration.Metadata;
import com.hpedrorodrigues.hyperion.definition.AbstractDefinition;
import com.hpedrorodrigues.hyperion.model.ClassDefinition;
import com.hpedrorodrigues.hyperion.model.Field;
import com.hpedrorodrigues.hyperion.model.JSONField;
import com.hpedrorodrigues.hyperion.model.enumeration.FieldType;
import com.hpedrorodrigues.hyperion.processor.base.AbstractProcessor;
import com.hpedrorodrigues.hyperion.type.base.AbstractType;
import com.hpedrorodrigues.hyperion.type.sealer.TypeSealer;
import com.hpedrorodrigues.hyperion.util.CollectionUtil;

import java.util.*;
import java.util.stream.Collectors;

public class DelegateParser<T extends AbstractType, D extends AbstractDefinition, P extends AbstractProcessor<T, D>> {

    private final P processor;

    private enum Type {
        PRIMITIVE, PRIMITIVE_ARRAY, OBJECT_ARRAY, OBJECT
    }

    public DelegateParser(final P processor) {
        this.processor = processor;
    }

    private D getDefinition() {
        return processor.getDefinition();
    }

    private T getType() {
        return processor.getType();
    }

    public List<ClassDefinition> parse(final Metadata metadata, final JsonElement json) {
        final Collection<JSONField> jsonFields = parseByType(
                metadata.getNamespace(),
                metadata.getRootEntityName(),
                json
        );

        final List<ClassDefinition> definitions = new ArrayList<>();
        processJsonFields(metadata, jsonFields, definitions, false);
        return definitions;
    }

    private Collection<JSONField> parseByType(final String namespace, final String name, final JsonElement json) {
        switch (getTypeByJSON(json)) {
            case PRIMITIVE:
                return Collections.singletonList(parsePrimitiveType(name, json));
            case PRIMITIVE_ARRAY:
                return Collections.singletonList(parsePrimitiveArrayType(name, json));
            case OBJECT_ARRAY:
                return parseObjectArrayType(namespace, name, json);
            default:
                return parseObjectType(namespace, name, json);
        }
    }

    private Collection<JSONField> parseObjectArrayType(final String namespace, final String name,
                                                       final JsonElement json) {
        final JsonElement head = json.getAsJsonArray().get(0);
        final Type type = getTypeByJSON(head);

        if (type.equals(Type.OBJECT)) {
            final List<JSONField> fields = new ArrayList<>();

            fields.add(parseObjectArrayType(name));
            fields.addAll(parseObjectType(namespace, name, head));

            return fields;
        } else if (type.equals(Type.PRIMITIVE_ARRAY)) {
            return Collections.singletonList(parsePrimitiveType(name, head));
        } else if (type.equals(Type.OBJECT_ARRAY)) {
            final List<JSONField> fields = new ArrayList<>();

            fields.add(parseObjectArrayType(name));
            fields.addAll(parseObjectArrayType(namespace, name, head));

            return fields;
        } else {
            return Collections.emptyList();
        }
    }

    private Collection<JSONField> parseObjectType(final String namespace, final String name, final JsonElement json) {
        final JSONField jsonField = new JSONField();
        jsonField.setType(FieldType.CLASS);

        final ClassDefinition definition = new ClassDefinition();
        definition.setNamespace(processor.getNamespace(namespace));
        definition.setStart(processor.getStartClass(name));

        for (final Map.Entry<String, JsonElement> entry : json.getAsJsonObject().entrySet()) {
            definition.addFields(parseByType(namespace, entry.getKey(), entry.getValue()));
        }

        definition.setEnd(getDefinition().getEndClass());
        definition.setFileName(processor.getFileName(name));

        jsonField.setClazz(definition);

        return Collections.singletonList(jsonField);
    }

    private JSONField parsePrimitiveType(final String name, final JsonElement json) {
        final String typeName = getPrimitiveType(json);
        final JSONField jsonField = new JSONField();

        final Field field = new Field();
        field.setDefinition(processor.getType(name, typeName));
        field.setGetter(processor.getGetter(name, typeName));
        field.setSetter(processor.getSetter(name, typeName));

        jsonField.setField(field);
        jsonField.setType(FieldType.FIELD);

        return jsonField;
    }

    private JSONField parsePrimitiveArrayType(final String name, final JsonElement json) {
        final JSONField jsonField = new JSONField();
        final JsonArray array = json.getAsJsonArray();
        final String typeName = array.size() == 0 ? getType().getText() : getPrimitiveType(array.get(0));

        final Field field = new Field();
        field.setDefinition(processor.getListType(name, typeName));
        field.setGetter(processor.getListGetter(name, typeName));
        field.setSetter(processor.getListSetter(name, typeName));

        jsonField.setField(field);
        jsonField.setType(FieldType.FIELD);

        return jsonField;
    }

    private JSONField parseObjectArrayType(final String name) {
        final JSONField jsonField = new JSONField();
        final String capitalized = processor.capitalize(name);

        final Field field = new Field();
        field.setDefinition(processor.getListType(name, capitalized));
        field.setGetter(processor.getListGetter(name, capitalized));
        field.setSetter(processor.getListSetter(name, capitalized));

        jsonField.setField(field);
        jsonField.setType(FieldType.FIELD);

        return jsonField;
    }

    private Type getTypeByJSON(final JsonElement json) {
        if (TypeSealer.isPrimitive(json) || json.isJsonNull()) {
            return Type.PRIMITIVE;
        } else if (TypeSealer.isArray(json)) {
            if (TypeSealer.isPrimitiveArray(json)) {
                return Type.PRIMITIVE_ARRAY;
            } else {
                return Type.OBJECT_ARRAY;
            }
        } else {
            return Type.OBJECT;
        }
    }

    protected String getPrimitiveType(final JsonElement json) {
        if (TypeSealer.isPrimitive(json) || json.isJsonNull()) {
            if (TypeSealer.isText(json)) {
                return getType().getText();
            } else if (TypeSealer.isInteger(json)) {
                return getType().getInteger();
            } else if (TypeSealer.isFloating(json)) {
                return getType().getFloating();
            } else {
                return getType().getBoolean();
            }
        }

        return null;
    }

    private void processJsonFields(final Metadata metadata,
                                   final Collection<JSONField> jsonFields,
                                   final List<ClassDefinition> allDefinitions,
                                   final boolean includeOnlyClasses) {
        if (CollectionUtil.isEmpty(jsonFields)) {
            return;
        }

        final List<ClassDefinition> definitions = jsonFields
                .stream()
                .filter(jsonField -> !includeOnlyClasses || jsonField.getType().equals(FieldType.CLASS))
                .map(jsonField -> {
                    if (jsonField.getType().equals(FieldType.FIELD)) {
                        return convertJsonFieldToClass(metadata, jsonField);
                    }

                    return processClassDefinition(metadata, jsonField.getClazz(), allDefinitions);
                }).collect(Collectors.toList());

        allDefinitions.addAll(definitions);
    }

    private Field processField(final Metadata metadata, final JSONField jsonField,
                               final List<ClassDefinition> allDefinitions) {
        if (jsonField.getType().equals(FieldType.FIELD)) {
            return jsonField.getField();
        }

        final Field field = new Field();

        final String fileName = jsonField.getClazz().getFileName();
        final String name = fileName.substring(0, fileName.indexOf("."));

        field.setDefinition(processor.getType(name, name));
        field.setGetter(processor.getGetter(name, name));
        field.setSetter(processor.getSetter(name, name));

        allDefinitions.add(processClassDefinition(metadata, jsonField.getClazz(), allDefinitions));
        processJsonFields(metadata, jsonField.getClazz().getJsonFields(), allDefinitions, true);

        return field;
    }

    private ClassDefinition convertJsonFieldToClass(final Metadata metadata, final JSONField jsonField) {
        final ClassDefinition definition = new ClassDefinition();

        definition.setNamespace(metadata.getNamespace());
        definition.setStart(processor.getStartClass(metadata.getRootEntityName()));

        definition.setFields(Collections.singletonList(jsonField.getField()));

        definition.setEnd(getDefinition().getEndClass());
        definition.setFileName(processor.getFileName(metadata.getRootEntityName()));

        return definition;
    }

    private ClassDefinition processClassDefinition(final Metadata metadata,
                                                   final ClassDefinition definition,
                                                   final List<ClassDefinition> definitions) {
        if (CollectionUtil.isEmpty(definition.getJsonFields())) {
            return definition;
        }

        final List<Field> fields = definition
                .getJsonFields()
                .stream()
                .map(jsonField -> processField(metadata, jsonField, definitions))
                .collect(Collectors.toList());

        definition.setFields(fields);
        definition.setJsonFields(null);

        return definition;
    }
}