package com.hpedrorodrigues.hyperion.parser.base;

import com.google.gson.JsonParser;
import com.hpedrorodrigues.hyperion.DefinitionConverter;
import com.hpedrorodrigues.hyperion.configuration.Metadata;
import com.hpedrorodrigues.hyperion.definition.AbstractDefinition;
import com.hpedrorodrigues.hyperion.model.PlainEntity;
import com.hpedrorodrigues.hyperion.processor.base.AbstractProcessor;
import com.hpedrorodrigues.hyperion.type.base.AbstractType;
import com.hpedrorodrigues.hyperion.util.CollectionUtil;

import java.util.List;
import java.util.stream.Collectors;

public abstract class AbstractParser<T extends AbstractType, D extends AbstractDefinition, P extends AbstractProcessor<T, D>> {

    protected final P processor;
    protected final JsonParser jsonParser;
    protected final DefinitionConverter definitionConverter;
    protected final DelegateParser<T, D, P> delegateParser;

    public AbstractParser(final P processor) {
        this.processor = processor;
        this.delegateParser = new DelegateParser<>(processor);
        this.jsonParser = new JsonParser();
        this.definitionConverter = new DefinitionConverter();
    }

    public List<PlainEntity> parse(final Metadata metadata) {
        return CollectionUtil
                .reverse(delegateParser.parse(metadata, jsonParser.parse(metadata.getJson())))
                .stream()
                .map(definitionConverter::toEntity)
                .collect(Collectors.toList());
    }

    protected final T getType() {
        return processor.getType();
    }

    protected final D getDefinition() {
        return processor.getDefinition();
    }
}