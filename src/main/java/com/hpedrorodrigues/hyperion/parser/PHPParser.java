package com.hpedrorodrigues.hyperion.parser;

import com.hpedrorodrigues.hyperion.definition.PHPDefinition;
import com.hpedrorodrigues.hyperion.parser.base.AbstractParser;
import com.hpedrorodrigues.hyperion.processor.PHPProcessor;
import com.hpedrorodrigues.hyperion.type.PHPType;

public class PHPParser extends AbstractParser<PHPType, PHPDefinition, PHPProcessor> {

    public PHPParser() {
        super(new PHPProcessor());
    }
}