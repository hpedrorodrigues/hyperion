package com.hpedrorodrigues.hyperion.parser;

import com.hpedrorodrigues.hyperion.definition.JavaDefinition;
import com.hpedrorodrigues.hyperion.parser.base.AbstractParser;
import com.hpedrorodrigues.hyperion.processor.JavaProcessor;
import com.hpedrorodrigues.hyperion.type.JavaType;

public class JavaParser extends AbstractParser<JavaType, JavaDefinition, JavaProcessor> {

    public JavaParser() {
        super(new JavaProcessor());
    }
}