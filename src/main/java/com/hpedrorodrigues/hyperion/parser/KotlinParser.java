package com.hpedrorodrigues.hyperion.parser;

import com.hpedrorodrigues.hyperion.definition.KotlinDefinition;
import com.hpedrorodrigues.hyperion.parser.base.AbstractParser;
import com.hpedrorodrigues.hyperion.processor.KotlinProcessor;
import com.hpedrorodrigues.hyperion.type.KotlinType;

public class KotlinParser extends AbstractParser<KotlinType, KotlinDefinition, KotlinProcessor> {

    public KotlinParser() {
        super(new KotlinProcessor());
    }
}