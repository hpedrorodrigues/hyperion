package com.hpedrorodrigues.hyperion.parser;

import com.hpedrorodrigues.hyperion.definition.CSharpDefinition;
import com.hpedrorodrigues.hyperion.parser.base.AbstractParser;
import com.hpedrorodrigues.hyperion.processor.CSharpProcessor;
import com.hpedrorodrigues.hyperion.type.CSharpType;

public class CSharpParser extends AbstractParser<CSharpType, CSharpDefinition, CSharpProcessor> {

    public CSharpParser() {
        super(new CSharpProcessor());
    }
}