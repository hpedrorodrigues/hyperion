package com.hpedrorodrigues.hyperion.parser;

import com.hpedrorodrigues.hyperion.definition.ScalaDefinition;
import com.hpedrorodrigues.hyperion.parser.base.AbstractParser;
import com.hpedrorodrigues.hyperion.processor.ScalaProcessor;
import com.hpedrorodrigues.hyperion.type.ScalaType;

public class ScalaParser extends AbstractParser<ScalaType, ScalaDefinition, ScalaProcessor> {

    public ScalaParser() {
        super(new ScalaProcessor());
    }
}