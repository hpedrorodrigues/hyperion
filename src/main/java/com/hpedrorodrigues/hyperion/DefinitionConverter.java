package com.hpedrorodrigues.hyperion;

import com.hpedrorodrigues.hyperion.model.ClassDefinition;
import com.hpedrorodrigues.hyperion.model.Field;
import com.hpedrorodrigues.hyperion.model.PlainEntity;
import com.hpedrorodrigues.hyperion.util.CollectionUtil;

import java.util.Optional;

public class DefinitionConverter {

    public PlainEntity toEntity(final ClassDefinition definition) {
        final StringBuilder builder = new StringBuilder();

        builder.append(definition.getNamespace())
                .append(definition.getStart())
                .append(getFieldDefinitions(definition))
                .append(getFieldGettersAndSetters(definition))
                .append(definition.getEnd());

        return new PlainEntity(definition.getFileName(), builder.toString());
    }

    private String getFieldDefinitions(final ClassDefinition definition) {
        if (CollectionUtil.isEmpty(definition.getFields())) {
            return "";
        }

        final Optional<String> fieldDefinitions = definition.getFields()
                .stream()
                .map(Field::getDefinition)
                .reduce((a, b) -> a + b);

        if (!fieldDefinitions.isPresent()) {
            throw new IllegalArgumentException("Invalid field definitions!");
        }

        return fieldDefinitions.get() + "\n";
    }

    private String getFieldGettersAndSetters(final ClassDefinition definition) {
        if (CollectionUtil.isEmpty(definition.getFields())) {
            return "";
        }

        final Optional<String> fieldDefinitions = definition
                .getFields()
                .stream()
                .map(field -> field.getGetter() + field.getSetter())
                .reduce((a, b) -> a + b);

        if (!fieldDefinitions.isPresent()) {
            throw new IllegalArgumentException("Invalid field definitions!");
        }

        return fieldDefinitions.get();
    }
}