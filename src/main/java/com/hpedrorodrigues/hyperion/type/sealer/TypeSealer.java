package com.hpedrorodrigues.hyperion.type.sealer;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;

public class TypeSealer {

    public static boolean isPrimitive(final JsonElement json) {
        return json.isJsonPrimitive();
    }

    public static boolean isInteger(final JsonElement json) {
        if (isPrimitive(json)) {

            JsonPrimitive primitive = json.getAsJsonPrimitive();

            if (primitive.isNumber()) {

                final double number = primitive.getAsDouble();
                return number % 2 == 0 || number % 2 == 1;
            }
        }

        return false;
    }

    public static boolean isFloating(final JsonElement json) {
        if (isPrimitive(json)) {

            JsonPrimitive primitive = json.getAsJsonPrimitive();

            if (primitive.isNumber()) {

                final double number = primitive.getAsDouble();
                return number % 2 != 0 && number % 2 != 1;
            }
        }

        return false;
    }

    public static boolean isText(final JsonElement json) {
        return json.isJsonNull() || (isPrimitive(json) && json.getAsJsonPrimitive().isString());
    }

    public static boolean isBoolean(final JsonElement json) {
        return isPrimitive(json) && json.getAsJsonPrimitive().isBoolean();
    }

    public static boolean isObject(final JsonElement json) {
        return json.isJsonObject();
    }

    public static boolean isArray(final JsonElement json) {
        return json.isJsonArray();
    }

    public static boolean isPrimitiveArray(final JsonElement json) {
        if (!isArray(json)) {
            return false;
        }

        final JsonArray array = json.getAsJsonArray();
        return array.size() == 0 || TypeSealer.isPrimitive(array.get(0));
    }
}