package com.hpedrorodrigues.hyperion.type;

import com.hpedrorodrigues.hyperion.type.base.AbstractType;

public abstract class StronglyTypified implements AbstractType {

    @Override
    public String getDefaultTextValue() {
        return null;
    }

    @Override
    public String getDefaultIntegerValue() {
        return null;
    }

    @Override
    public String getDefaultFloatingValue() {
        return null;
    }

    @Override
    public String getDefaultBooleanValue() {
        return null;
    }

    @Override
    public String getDefaultNullValue() {
        return null;
    }

    @Override
    public String getDefaultNullListValue() {
        return null;
    }
}