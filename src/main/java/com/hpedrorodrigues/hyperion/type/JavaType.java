package com.hpedrorodrigues.hyperion.type;

public class JavaType extends StronglyTypified {

    @Override
    public String getInteger() {
        return "Integer";
    }

    @Override
    public String getFloating() {
        return "Float";
    }

    @Override
    public String getText() {
        return "String";
    }

    @Override
    public String getBoolean() {
        return "Boolean";
    }
}