package com.hpedrorodrigues.hyperion.type.base;

public interface AbstractType {

    String getInteger();

    String getFloating();

    String getText();

    String getBoolean();

    String getDefaultIntegerValue();

    String getDefaultFloatingValue();

    String getDefaultTextValue();

    String getDefaultBooleanValue();

    String getDefaultNullValue();

    String getDefaultNullListValue();
}