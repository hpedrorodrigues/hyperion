package com.hpedrorodrigues.hyperion.type;

public class CSharpType extends StronglyTypified {

    @Override
    public String getInteger() {
        return "int";
    }

    @Override
    public String getFloating() {
        return "float";
    }

    @Override
    public String getText() {
        return "string";
    }

    @Override
    public String getBoolean() {
        return "bool";
    }
}