package com.hpedrorodrigues.hyperion.type;

import com.hpedrorodrigues.hyperion.type.base.AbstractType;

public class ScalaType implements AbstractType {

    @Override
    public String getInteger() {
        return "Int";
    }

    @Override
    public String getFloating() {
        return "Float";
    }

    @Override
    public String getText() {
        return "String";
    }

    @Override
    public String getBoolean() {
        return "Boolean";
    }

    @Override
    public String getDefaultIntegerValue() {
        return "0";
    }

    @Override
    public String getDefaultFloatingValue() {
        return "0.0";
    }

    @Override
    public String getDefaultTextValue() {
        return "\"\"";
    }

    @Override
    public String getDefaultBooleanValue() {
        return "false";
    }

    @Override
    public String getDefaultNullValue() {
        return "null";
    }

    @Override
    public String getDefaultNullListValue() {
        return "Nil";
    }
}