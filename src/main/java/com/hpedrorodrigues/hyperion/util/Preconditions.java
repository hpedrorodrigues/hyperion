package com.hpedrorodrigues.hyperion.util;

public class Preconditions {

    private Preconditions() {
    }

    public static <T> T notNull(final T reference) {
        return notNull(reference, "Required field cannot be null.");
    }

    public static <T> T notNull(final T reference, final String message) {
        if (reference == null) {
            throw new NullPointerException(message);
        }

        return reference;
    }
}