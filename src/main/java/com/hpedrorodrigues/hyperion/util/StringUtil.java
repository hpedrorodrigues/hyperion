package com.hpedrorodrigues.hyperion.util;

public class StringUtil {

    public static String capitalize(final String value) {
        String result = "";
        boolean capitalize = true;

        for (final char c : value.toCharArray()) {
            final boolean isLetter = Character.isLetter(c);

            if (isLetter) {
                result += (capitalize || Character.isUpperCase(c))
                        ? Character.toUpperCase(c) : Character.toLowerCase(c);
            }

            capitalize = !isLetter;
        }

        return result;
    }

    public static String decapitalize(final String value) {
        String result = "";
        boolean capitalize = false, isFirstLetter = true;

        for (final char c : value.toCharArray()) {
            final boolean isLetter = Character.isLetter(c);

            if (isLetter) {
                result += (!isFirstLetter && (capitalize || Character.isUpperCase(c)))
                        ? Character.toUpperCase(c) : Character.toLowerCase(c);

                if (isFirstLetter) {
                    isFirstLetter = false;
                }
            }

            capitalize = !isLetter;
        }

        return result;
    }
}