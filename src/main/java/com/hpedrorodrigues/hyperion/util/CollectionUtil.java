package com.hpedrorodrigues.hyperion.util;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class CollectionUtil {

    public static boolean isEmpty(final Collection<?> collection) {
        return collection == null || collection.isEmpty();
    }

    public static <T> List<T> reverse(final List<T> list) {
        Collections.reverse(list);
        return list;
    }
}