package com.hpedrorodrigues.hyperion.util;

import com.google.gson.JsonElement;
import com.hpedrorodrigues.hyperion.type.sealer.TypeSealer;

public class JsonUtil {

    public static boolean isEmpty(final JsonElement json) {
        if (json == null) {
            return true;
        } else if (json.isJsonArray()) {
            return json.getAsJsonArray().size() == 0;
        } else if (json.isJsonObject()) {
            return json.getAsJsonObject().entrySet().size() == 0;
        }

        return false;
    }

    public static boolean isArrayNotEmpty(final JsonElement json) {
        return TypeSealer.isArray(json) && !isEmpty(json);
    }

    public static JsonElement head(final JsonElement json) {
        if (!TypeSealer.isArray(json) || isEmpty(json)) {
            return null;
        }

        return json.getAsJsonArray().get(0);
    }
}