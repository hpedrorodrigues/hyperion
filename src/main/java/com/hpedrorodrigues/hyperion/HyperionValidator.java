package com.hpedrorodrigues.hyperion;

import com.hpedrorodrigues.hyperion.configuration.Metadata;
import com.hpedrorodrigues.hyperion.util.Preconditions;

public class HyperionValidator {

    public void validate(final Metadata metadata) {
        Preconditions.notNull(metadata, "Metadata cannot be null!");
        Preconditions.notNull(metadata.getLanguage(), "Language field cannot be null!");
        Preconditions.notNull(metadata.getJson(), "JSON field cannot be null!");
    }
}