package com.hpedrorodrigues.hyperion.configuration;

import com.hpedrorodrigues.hyperion.model.enumeration.Language;

public class Metadata {

    public static final String DEFAULT_PACKAGE = "com.example";
    public static final String DEFAULT_ENTITY = "Example";

    private Language language;
    private String namespace;
    private String rootEntityName;
    private String json;

    public Metadata() {
    }

    public Metadata(final Language language, final String json) {
        this.language = language;
        this.json = json;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public String getNamespace() {
        if (namespace == null || namespace.isEmpty()) {
            return DEFAULT_PACKAGE;
        }

        return namespace;
    }

    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    public String getRootEntityName() {
        if (rootEntityName == null || rootEntityName.isEmpty()) {
            return DEFAULT_ENTITY;
        }

        return rootEntityName;
    }

    public void setRootEntityName(String rootEntityName) {
        this.rootEntityName = rootEntityName;
    }

    public String getJson() {
        return json;
    }

    public void setJson(String json) {
        this.json = json;
    }
}