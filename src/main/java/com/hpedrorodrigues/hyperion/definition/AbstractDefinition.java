package com.hpedrorodrigues.hyperion.definition;

import com.hpedrorodrigues.hyperion.model.enumeration.Language;

public abstract class AbstractDefinition {

    public abstract String getSelfReference();

    public abstract String getDelimiter();

    public abstract String getEndClass();

    public abstract String getEndLine();

    public abstract String getExtension();

    public abstract Language getLanguage();

    public String getTab() {
        return "\t";
    }

    public String getBreakLine() {
        return "\n";
    }
}