package com.hpedrorodrigues.hyperion.definition;

import com.hpedrorodrigues.hyperion.model.enumeration.Language;

public class CSharpDefinition extends AbstractDefinition {

    @Override
    public String getSelfReference() {
        return null;
    }

    @Override
    public String getDelimiter() {
        return ".";
    }

    @Override
    public String getEndClass() {
        return new StringBuilder()
                .append(getBreakLine())
                .append(getTab())
                .append("}")
                .append(getBreakLine())
                .append("}")
                .toString();
    }

    @Override
    public String getEndLine() {
        return ";";
    }

    @Override
    public String getExtension() {
        return "cs";
    }

    @Override
    public Language getLanguage() {
        return Language.CSHARP;
    }
}