package com.hpedrorodrigues.hyperion.definition;

import com.hpedrorodrigues.hyperion.model.enumeration.Language;

public class KotlinDefinition extends AbstractDefinition {

    @Override
    public String getSelfReference() {
        return "this";
    }

    @Override
    public String getDelimiter() {
        return ".";
    }

    @Override
    public String getEndClass() {
        return new StringBuilder()
                .append(getBreakLine())
                .append("}")
                .toString();
    }

    @Override
    public String getEndLine() {
        return null;
    }

    @Override
    public String getExtension() {
        return "kt";
    }

    @Override
    public Language getLanguage() {
        return Language.KOTLIN;
    }
}