package com.hpedrorodrigues.hyperion.definition;

import com.hpedrorodrigues.hyperion.model.enumeration.Language;

public class PHPDefinition extends AbstractDefinition {

    @Override
    public String getSelfReference() {
        return "$this";
    }

    @Override
    public String getDelimiter() {
        return "->";
    }

    @Override
    public String getEndClass() {
        return new StringBuilder()
                .append(getBreakLine())
                .append("}")
                .toString();
    }

    @Override
    public String getEndLine() {
        return ";";
    }

    @Override
    public String getExtension() {
        return "php";
    }

    @Override
    public Language getLanguage() {
        return Language.PHP;
    }
}