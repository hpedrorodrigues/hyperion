package com.hpedrorodrigues.hyperion;

import com.hpedrorodrigues.hyperion.configuration.Metadata;
import com.hpedrorodrigues.hyperion.model.PlainEntity;
import com.hpedrorodrigues.hyperion.model.enumeration.Language;
import com.hpedrorodrigues.hyperion.parser.*;
import com.hpedrorodrigues.hyperion.parser.base.AbstractParser;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Hyperion {

    private static volatile Hyperion singleton = null;

    private final Map<Language, AbstractParser<?, ?, ?>> parsers;
    private final HyperionValidator validator;

    private Hyperion() {
        this.parsers = createParsers();
        this.validator = new HyperionValidator();
    }

    /**
     * Create singleton if needed.
     *
     * @return hyperion instance
     */
    private static synchronized Hyperion get() {
        if (singleton == null) {
            singleton = new Hyperion();
        }

        return singleton;
    }

    /**
     * Responsible for parse json toEntity to informed language.
     *
     * @param language language to be used in the parser
     * @param json     json to be parsed
     * @return a list of plain entities
     * @see Language
     * @see PlainEntity
     * @since 1.1
     */
    public static List<PlainEntity> parse(final Language language, final String json) {
        return parse(new Metadata(language, json));
    }

    /**
     * Responsible for parse json toEntity to informed language.
     *
     * @param metadata configuration containing all necessary fields
     * @return a list of plain entities
     * @see Language
     * @see PlainEntity
     * @since 1.0
     */
    public static List<PlainEntity> parse(final Metadata metadata) {
        get().validator.validate(metadata);
        return get().parsers.get(metadata.getLanguage()).parse(metadata);
    }

    private Map<Language, AbstractParser<?, ?, ?>> createParsers() {
        final Map<Language, AbstractParser<?, ?, ?>> parsers = new HashMap<>();

        parsers.put(Language.JAVA, new JavaParser());
        parsers.put(Language.SCALA, new ScalaParser());
        parsers.put(Language.KOTLIN, new KotlinParser());
        parsers.put(Language.CSHARP, new CSharpParser());
        parsers.put(Language.PHP, new PHPParser());

        return parsers;
    }
}