package com.hpedrorodrigues.hyperion.parser.base;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.hpedrorodrigues.hyperion.configuration.Metadata;
import com.hpedrorodrigues.hyperion.definition.JavaDefinition;
import com.hpedrorodrigues.hyperion.model.ClassDefinition;
import com.hpedrorodrigues.hyperion.model.Field;
import com.hpedrorodrigues.hyperion.model.enumeration.Language;
import com.hpedrorodrigues.hyperion.processor.JavaProcessor;
import com.hpedrorodrigues.hyperion.type.JavaType;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.List;

@RunWith(JUnit4.class)
public class DelegateParserTest {

    private DelegateParser<JavaType, JavaDefinition, JavaProcessor> delegateParser;

    private JavaType type;
    private JsonParser jsonParser;

    @Before
    public void setUp() {
        delegateParser = new DelegateParser<>(new JavaProcessor());

        type = new JavaType();
        jsonParser = new JsonParser();
    }

    @Test
    public void validatePrimitiveIntegerType() {
        final String jsonText = "1";
        final JsonElement json = jsonParser.parse(jsonText);

        Assertions.assertThat(delegateParser.getPrimitiveType(json)).isEqualTo(type.getInteger());
    }

    @Test
    public void validatePrimitiveFloatingType() {
        final String jsonText = "1.2";
        final JsonElement json = jsonParser.parse(jsonText);

        Assertions.assertThat(delegateParser.getPrimitiveType(json)).isEqualTo(type.getFloating());
    }

    @Test
    public void validatePrimitiveTextType() {
        final String jsonText = "abcd";
        final JsonElement json = jsonParser.parse(jsonText);

        Assertions.assertThat(delegateParser.getPrimitiveType(json)).isEqualTo(type.getText());
    }

    @Test
    public void validateNullType() {
        final String jsonText = "null";
        final JsonElement json = jsonParser.parse(jsonText);

        Assertions.assertThat(delegateParser.getPrimitiveType(json)).isEqualTo(type.getText());
    }

    @Test
    public void validatePrimitiveBooleanType() {
        final String jsonText = "true";
        final JsonElement json = jsonParser.parse(jsonText);

        Assertions.assertThat(delegateParser.getPrimitiveType(json)).isEqualTo(type.getBoolean());
    }

    @Test
    public void parsePrimitiveArray() {
        final Metadata metadata = new Metadata();
        metadata.setLanguage(Language.JAVA);
        metadata.setRootEntityName("test");
        metadata.setJson("[1, 2, 3, 4]");

        final List<ClassDefinition> parsed = delegateParser.parse(metadata, jsonParser.parse(metadata.getJson()));
        Assertions.assertThat(parsed).isNotEmpty();
        Assertions.assertThat(parsed.size()).isEqualTo(1);

        final ClassDefinition head = parsed.get(0);
        Assertions.assertThat(head.getFileName()).isEqualTo("Test.java");

        Assertions.assertThat(head.getFields()).isNotEmpty();
        Assertions.assertThat(head.getFields().size()).isEqualTo(1);

        final Field headField = head.getFields().get(0);
        Assertions.assertThat(headField.getDefinition())
                .isEqualTo("\n\tprivate List<Integer> test;");
        Assertions.assertThat(headField.getGetter())
                .isEqualTo("\n\tpublic List<Integer> getTest() {\n\t\treturn this.test;\n\t}\n");
        Assertions.assertThat(headField.getSetter())
                .isEqualTo("\n\tpublic void setTest(List<Integer> test) {\n\t\tthis.test = test;\n\t}\n");
    }

    @Test
    public void parseArrayOfObjects() {
        final Metadata metadata = new Metadata();
        metadata.setLanguage(Language.JAVA);
        metadata.setRootEntityName("test");
        metadata.setJson("[{\"field\": 0}]");

        final List<ClassDefinition> parsed = delegateParser.parse(metadata, jsonParser.parse(metadata.getJson()));
        Assertions.assertThat(parsed).isNotEmpty();
        Assertions.assertThat(parsed.size()).isEqualTo(2);

        final ClassDefinition head = parsed.get(0);
        Assertions.assertThat(head.getFileName()).isEqualTo("Test.java");

        Assertions.assertThat(head.getFields()).isNotEmpty();
        Assertions.assertThat(head.getFields().size()).isEqualTo(1);

        final Field first = head.getFields().get(0);
        Assertions.assertThat(first.getDefinition())
                .isEqualTo("\n\tprivate List<Test> test;");
        Assertions.assertThat(first.getGetter())
                .isEqualTo("\n\tpublic List<Test> getTest() {\n\t\treturn this.test;\n\t}\n");
        Assertions.assertThat(first.getSetter())
                .isEqualTo("\n\tpublic void setTest(List<Test> test) {\n\t\tthis.test = test;\n\t}\n");

        final ClassDefinition tail = parsed.get(1);
        Assertions.assertThat(tail.getFileName()).isEqualTo("Test.java");

        Assertions.assertThat(tail.getFields()).isNotEmpty();
        Assertions.assertThat(tail.getFields().size()).isEqualTo(1);

        final Field second = tail.getFields().get(0);
        Assertions.assertThat(second.getDefinition())
                .isEqualTo("\n\tprivate Integer field;");
        Assertions.assertThat(second.getGetter())
                .isEqualTo("\n\tpublic Integer getField() {\n\t\treturn this.field;\n\t}\n");
        Assertions.assertThat(second.getSetter())
                .isEqualTo("\n\tpublic void setField(Integer field) {\n\t\tthis.field = field;\n\t}\n");
    }

    @Test
    public void parseArrayOfComplexObjects() {
        final String json = "[{\"friends\": [{\"field\": 0}]}]";

        final Metadata metadata = new Metadata();
        metadata.setLanguage(Language.JAVA);
        metadata.setRootEntityName("test");
        metadata.setJson(json);

        final List<ClassDefinition> parsed = delegateParser.parse(metadata, jsonParser.parse(metadata.getJson()));
        Assertions.assertThat(parsed).isNotEmpty();
        Assertions.assertThat(parsed.size()).isEqualTo(3);

        final ClassDefinition head = parsed.get(0);
        Assertions.assertThat(head.getFileName()).isEqualTo("Friends.java");

        Assertions.assertThat(head.getFields()).isNotEmpty();
        Assertions.assertThat(head.getFields().size()).isEqualTo(1);

        final Field first = head.getFields().get(0);
        Assertions.assertThat(first.getDefinition())
                .isEqualTo("\n\tprivate Integer field;");
        Assertions.assertThat(first.getGetter())
                .isEqualTo("\n\tpublic Integer getField() {\n\t\treturn this.field;\n\t}\n");
        Assertions.assertThat(first.getSetter())
                .isEqualTo("\n\tpublic void setField(Integer field) {\n\t\tthis.field = field;\n\t}\n");

        final ClassDefinition half = parsed.get(1);
        Assertions.assertThat(half.getFileName()).isEqualTo("Test.java");

        Assertions.assertThat(half.getFields()).isNotEmpty();
        Assertions.assertThat(half.getFields().size()).isEqualTo(1);

        final Field second = half.getFields().get(0);
        Assertions.assertThat(second.getDefinition())
                .isEqualTo("\n\tprivate List<Test> test;");
        Assertions.assertThat(second.getGetter())
                .isEqualTo("\n\tpublic List<Test> getTest() {\n\t\treturn this.test;\n\t}\n");
        Assertions.assertThat(second.getSetter())
                .isEqualTo("\n\tpublic void setTest(List<Test> test) {\n\t\tthis.test = test;\n\t}\n");

        final ClassDefinition tail = parsed.get(2);
        Assertions.assertThat(tail.getFileName()).isEqualTo("Test.java");

        Assertions.assertThat(tail.getFields()).isNotEmpty();

        final Field third = tail.getFields().get(0);
        Assertions.assertThat(third.getDefinition())
                .isEqualTo("\n\tprivate List<Friends> friends;");
        Assertions.assertThat(third.getGetter())
                .isEqualTo("\n\tpublic List<Friends> getFriends() {\n\t\treturn this.friends;\n\t}\n");
        Assertions.assertThat(third.getSetter())
                .isEqualTo("\n\tpublic void setFriends(List<Friends> friends) {\n\t\tthis.friends = friends;\n\t}\n");
    }

    @Test
    public void parseEmptyArray() {
        final Metadata metadata = new Metadata();
        metadata.setLanguage(Language.JAVA);
        metadata.setRootEntityName("test");
        metadata.setJson("[]");

        final List<ClassDefinition> parsed = delegateParser.parse(metadata, jsonParser.parse(metadata.getJson()));
        Assertions.assertThat(parsed).isNotEmpty();
        Assertions.assertThat(parsed.size()).isEqualTo(1);

        final ClassDefinition head = parsed.get(0);
        Assertions.assertThat(head.getFileName()).isEqualTo("Test.java");

        Assertions.assertThat(head.getFields()).isNotEmpty();
        Assertions.assertThat(head.getFields().size()).isEqualTo(1);

        final Field field = head.getFields().get(0);
        Assertions.assertThat(field.getDefinition())
                .isEqualTo("\n\tprivate List<String> test;");
        Assertions.assertThat(field.getGetter())
                .isEqualTo("\n\tpublic List<String> getTest() {\n\t\treturn this.test;\n\t}\n");
        Assertions.assertThat(field.getSetter())
                .isEqualTo("\n\tpublic void setTest(List<String> test) {\n\t\tthis.test = test;\n\t}\n");
    }

    @Test
    public void parseObject() {
        final Metadata metadata = new Metadata();
        metadata.setLanguage(Language.JAVA);
        metadata.setRootEntityName("test");
        metadata.setJson("{\"a\": \"a\"}");

        final List<ClassDefinition> parsed = delegateParser.parse(metadata, jsonParser.parse(metadata.getJson()));
        Assertions.assertThat(parsed).isNotEmpty();
        Assertions.assertThat(parsed.size()).isEqualTo(1);

        final ClassDefinition head = parsed.get(0);
        Assertions.assertThat(head.getFileName()).isEqualTo("Test.java");

        Assertions.assertThat(head.getFields()).isNotEmpty();
        Assertions.assertThat(head.getFields().size()).isEqualTo(1);

        final Field field = head.getFields().get(0);
        Assertions.assertThat(field.getDefinition())
                .isEqualTo("\n\tprivate String a;");
        Assertions.assertThat(field.getGetter())
                .isEqualTo("\n\tpublic String getA() {\n\t\treturn this.a;\n\t}\n");
        Assertions.assertThat(field.getSetter())
                .isEqualTo("\n\tpublic void setA(String a) {\n\t\tthis.a = a;\n\t}\n");
    }

    @Test
    public void parseEmptyObject() {
        final Metadata metadata = new Metadata();
        metadata.setLanguage(Language.JAVA);
        metadata.setRootEntityName("test");
        metadata.setJson("{}");

        final List<ClassDefinition> parsed = delegateParser.parse(metadata, jsonParser.parse(metadata.getJson()));
        Assertions.assertThat(parsed).isNotEmpty();
        Assertions.assertThat(parsed.size()).isEqualTo(1);

        final ClassDefinition head = parsed.get(0);
        Assertions.assertThat(head.getFileName()).isEqualTo("Test.java");
        Assertions.assertThat(head.getFields()).isNull();
    }

    @Test
    public void parseNullObject() {
        final Metadata metadata = new Metadata();
        metadata.setLanguage(Language.JAVA);
        metadata.setRootEntityName("test");
        metadata.setJson("{\"a\": null}");

        final List<ClassDefinition> parsed = delegateParser.parse(metadata, jsonParser.parse(metadata.getJson()));
        Assertions.assertThat(parsed).isNotEmpty();
        Assertions.assertThat(parsed.size()).isEqualTo(1);

        final ClassDefinition head = parsed.get(0);
        Assertions.assertThat(head.getFileName()).isEqualTo("Test.java");

        Assertions.assertThat(head.getFields()).isNotEmpty();
        Assertions.assertThat(head.getFields().size()).isEqualTo(1);

        final Field field = head.getFields().get(0);
        Assertions.assertThat(field.getDefinition())
                .isEqualTo("\n\tprivate String a;");
        Assertions.assertThat(field.getGetter())
                .isEqualTo("\n\tpublic String getA() {\n\t\treturn this.a;\n\t}\n");
        Assertions.assertThat(field.getSetter())
                .isEqualTo("\n\tpublic void setA(String a) {\n\t\tthis.a = a;\n\t}\n");
    }
}