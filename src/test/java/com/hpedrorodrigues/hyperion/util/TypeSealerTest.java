package com.hpedrorodrigues.hyperion.util;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.hpedrorodrigues.hyperion.base.AbstractTest;
import com.hpedrorodrigues.hyperion.type.sealer.TypeSealer;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class TypeSealerTest extends AbstractTest {

    private JsonParser parser;

    @Before
    public void setUp() {
        this.parser = new JsonParser();
    }

    @Test
    public void validateTypeInferenceForBoolean() {
        final String jsonText = "true";
        final JsonElement json = parser.parse(jsonText);

        Assertions.assertThat(TypeSealer.isBoolean(json)).isTrue();
        Assertions.assertThat(TypeSealer.isPrimitive(json)).isTrue();
        Assertions.assertThat(TypeSealer.isArray(json)).isFalse();
        Assertions.assertThat(TypeSealer.isFloating(json)).isFalse();
        Assertions.assertThat(TypeSealer.isInteger(json)).isFalse();
        Assertions.assertThat(TypeSealer.isObject(json)).isFalse();
        Assertions.assertThat(TypeSealer.isText(json)).isFalse();
    }

    @Test
    public void validateTypeInferenceForInteger() {
        final String jsonText = "1";
        final JsonElement json = parser.parse(jsonText);

        Assertions.assertThat(TypeSealer.isInteger(json)).isTrue();
        Assertions.assertThat(TypeSealer.isPrimitive(json)).isTrue();
        Assertions.assertThat(TypeSealer.isBoolean(json)).isFalse();
        Assertions.assertThat(TypeSealer.isArray(json)).isFalse();
        Assertions.assertThat(TypeSealer.isFloating(json)).isFalse();
        Assertions.assertThat(TypeSealer.isObject(json)).isFalse();
        Assertions.assertThat(TypeSealer.isText(json)).isFalse();
    }

    @Test
    public void validateTypeInferenceForFloatingPoint() {
        final String jsonText = "2.3";
        final JsonElement json = parser.parse(jsonText);

        Assertions.assertThat(TypeSealer.isFloating(json)).isTrue();
        Assertions.assertThat(TypeSealer.isPrimitive(json)).isTrue();
        Assertions.assertThat(TypeSealer.isBoolean(json)).isFalse();
        Assertions.assertThat(TypeSealer.isArray(json)).isFalse();
        Assertions.assertThat(TypeSealer.isInteger(json)).isFalse();
        Assertions.assertThat(TypeSealer.isObject(json)).isFalse();
        Assertions.assertThat(TypeSealer.isText(json)).isFalse();
    }

    @Test
    public void validateTypeInferenceForText() {
        final String jsonText = "\"word\"";
        final JsonElement json = parser.parse(jsonText);

        Assertions.assertThat(TypeSealer.isText(json)).isTrue();
        Assertions.assertThat(TypeSealer.isPrimitive(json)).isTrue();
        Assertions.assertThat(TypeSealer.isBoolean(json)).isFalse();
        Assertions.assertThat(TypeSealer.isArray(json)).isFalse();
        Assertions.assertThat(TypeSealer.isInteger(json)).isFalse();
        Assertions.assertThat(TypeSealer.isFloating(json)).isFalse();
        Assertions.assertThat(TypeSealer.isObject(json)).isFalse();
    }

    @Test
    public void validateTypeInferenceForObject() {
        final String jsonText = "{\"key\": \"word\"}";
        final JsonElement json = parser.parse(jsonText);

        Assertions.assertThat(TypeSealer.isObject(json)).isTrue();
        Assertions.assertThat(TypeSealer.isPrimitive(json)).isFalse();
        Assertions.assertThat(TypeSealer.isBoolean(json)).isFalse();
        Assertions.assertThat(TypeSealer.isArray(json)).isFalse();
        Assertions.assertThat(TypeSealer.isInteger(json)).isFalse();
        Assertions.assertThat(TypeSealer.isFloating(json)).isFalse();
        Assertions.assertThat(TypeSealer.isText(json)).isFalse();
    }

    @Test
    public void validateTypeInferenceForArray() {
        final String jsonText = "[1, 2, 3, 4]";
        final JsonElement json = parser.parse(jsonText);

        Assertions.assertThat(TypeSealer.isArray(json)).isTrue();
        Assertions.assertThat(TypeSealer.isObject(json)).isFalse();
        Assertions.assertThat(TypeSealer.isPrimitive(json)).isFalse();
        Assertions.assertThat(TypeSealer.isBoolean(json)).isFalse();
        Assertions.assertThat(TypeSealer.isInteger(json)).isFalse();
        Assertions.assertThat(TypeSealer.isFloating(json)).isFalse();
        Assertions.assertThat(TypeSealer.isText(json)).isFalse();
    }

    @Test
    public void validateTypeInferenceForNullValue() {
        final String jsonText = "";
        final JsonElement json = parser.parse(jsonText);

        Assertions.assertThat(TypeSealer.isText(json)).isTrue();
        Assertions.assertThat(TypeSealer.isPrimitive(json)).isFalse();
        Assertions.assertThat(TypeSealer.isBoolean(json)).isFalse();
        Assertions.assertThat(TypeSealer.isArray(json)).isFalse();
        Assertions.assertThat(TypeSealer.isInteger(json)).isFalse();
        Assertions.assertThat(TypeSealer.isFloating(json)).isFalse();
        Assertions.assertThat(TypeSealer.isObject(json)).isFalse();
    }
}