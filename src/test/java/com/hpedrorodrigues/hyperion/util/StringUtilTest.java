package com.hpedrorodrigues.hyperion.util;

import com.hpedrorodrigues.hyperion.base.AbstractTest;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.Arrays;

@RunWith(JUnit4.class)
public class StringUtilTest extends AbstractTest {

    @Test
    public void validateCapitalizeSingleWord() {
        final String[] names = new String[]{"Test", "test", "_test", "test_", "_test_"};
        final String expected = "Test";

        Arrays.stream(names).forEach(name ->
                Assertions.assertThat(StringUtil.capitalize(name)).isEqualTo(expected));
    }

    @Test
    public void validateCapitalizeMultipleWords() {
        final String[] names = new String[]{
                "TestTest", "test_test", "testTest", "_testTest", "TestTest_", "_testTest_"
        };
        final String expected = "TestTest";

        Arrays.stream(names).forEach(name ->
                Assertions.assertThat(StringUtil.capitalize(name)).isEqualTo(expected));
    }

    @Test
    public void validateDecapitalizeSingleWord() {
        final String[] names = new String[]{"Test", "test", "_test", "test_", "_test_"};
        final String expected = "test";

        Arrays.stream(names).forEach(name ->
                Assertions.assertThat(StringUtil.decapitalize(name)).isEqualTo(expected));
    }

    @Test
    public void validateDecapitalizeMultipleWords() {
        final String[] names = new String[]{
                "TestTest", "Test_test", "_testTest", "TestTest_", "_testTest_"
        };
        final String expected = "testTest";

        Arrays.stream(names).forEach(name ->
                Assertions.assertThat(StringUtil.decapitalize(name)).isEqualTo(expected));
    }
}