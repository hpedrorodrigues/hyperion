package com.hpedrorodrigues.hyperion.util;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.hpedrorodrigues.hyperion.base.AbstractTest;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class JsonUtilTest extends AbstractTest {

    private JsonParser parser;

    @Before
    public void setUp() {
        this.parser = new JsonParser();
    }

    @Test
    public void returnTrueWhenIsPassedAEmptyJsonArray() {
        final JsonElement json = parser.parse("[]");

        Assertions.assertThat(JsonUtil.isEmpty(json)).isTrue();
    }

    @Test
    public void returnFalseWhenIsPassedANonEmptyJsonArray() {
        final JsonElement json = parser.parse("[1, 2, 3, 4, 5]");

        Assertions.assertThat(JsonUtil.isEmpty(json)).isFalse();
    }

    @Test
    public void returnTrueWhenIsPassedAEmptyJsonObject() {
        final JsonElement json = parser.parse("{}");

        Assertions.assertThat(JsonUtil.isEmpty(json)).isTrue();
    }

    @Test
    public void returnFalseWhenIsPassedANonEmptyJsonObject() {
        final JsonElement json = parser.parse("{\"a\": \"a\"}");

        Assertions.assertThat(JsonUtil.isEmpty(json)).isFalse();
    }

    @Test
    public void returnTrueWhenIsPassedANullJson() {
        Assertions.assertThat(JsonUtil.isEmpty(null)).isTrue();
    }

    @Test
    public void returnFalseWhenIsPassedAPrimitiveJson() {
        final JsonElement json = parser.parse("1");

        Assertions.assertThat(JsonUtil.isEmpty(json)).isFalse();
    }

    @Test
    public void returnFirstElementWhenArrayIsNotEmpty() {
        final JsonElement json = parser.parse("[1, 2, 3, 4, 5]");

        Assertions.assertThat(JsonUtil.head(json).getAsInt()).isEqualTo(1);
    }

    @Test
    public void returnNullWhenArrayIsEmpty() {
        final JsonElement json = parser.parse("[]");

        Assertions.assertThat(JsonUtil.head(json)).isNull();
    }

    @Test
    public void returnNullWhenJsonIsAObject() {
        final JsonElement json = parser.parse("{}");

        Assertions.assertThat(JsonUtil.head(json)).isNull();
    }

    @Test
    public void returnTrueWhenJsonIsANonEmptyArray() {
        final JsonElement json = parser.parse("[1]");

        Assertions.assertThat(JsonUtil.isArrayNotEmpty(json)).isTrue();
    }

    @Test
    public void returnFalseWhenJsonIsAObject() {
        final JsonElement json = parser.parse("{}");

        Assertions.assertThat(JsonUtil.isArrayNotEmpty(json)).isFalse();
    }
}