package com.hpedrorodrigues.hyperion.processor;

import com.hpedrorodrigues.hyperion.base.AbstractTest;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class ScalaProcessorTest extends AbstractTest {

    private ScalaProcessor processor;

    @Before
    public void setUp() {
        this.processor = new ScalaProcessor();
    }

    @Test
    public void processFileName() {
        final String expected = "Test.scala";

        Assertions
                .assertThat(processor.getFileName("test"))
                .isEqualTo(expected);
    }

    @Test
    public void processPackageName() {
        final String expected = "package test\n\n";

        Assertions
                .assertThat(processor.getNamespace("test"))
                .isEqualTo(expected);
    }

    @Test
    public void processStartClass() {
        final String expected = "public class Test {\n\n";

        Assertions
                .assertThat(processor.getStartClass("test"))
                .isEqualTo(expected);
    }

    @Test
    public void processType() {
        final String expected = "\tprivate var _test: Int = 0\n";

        Assertions
                .assertThat(processor.getType("test", "Int"))
                .isEqualTo(expected);
    }

    @Test
    public void processListType() {
        final String expected = "\tprivate var _test: List[String] = Nil\n";

        Assertions
                .assertThat(processor.getListType("test", "String"))
                .isEqualTo(expected);
    }

    @Test
    public void processListTypeName() {
        final String expected = "List[String]";

        Assertions
                .assertThat(processor.getListTypeName("String"))
                .isEqualTo(expected);
    }

    @Test
    public void processListSetter() {
        final String expected = "\tdef test_= (value: List[Double]): Unit = _test = value\n\n";

        Assertions
                .assertThat(processor.getListSetter("test", "Double"))
                .isEqualTo(expected);
    }

    @Test
    public void processListGetter() {
        final String expected = "\tdef test = _test\n";

        Assertions
                .assertThat(processor.getListGetter("test", "Boolean"))
                .isEqualTo(expected);
    }

    @Test
    public void processSetter() {
        final String expected = "\tdef test_= (value: Float): Unit = _test = value\n\n";

        Assertions
                .assertThat(processor.getSetter("test", "Float"))
                .isEqualTo(expected);
    }

    @Test
    public void processGetter() {
        final String expected = "\tdef test = _test\n";

        Assertions
                .assertThat(processor.getGetter("test", "Integer"))
                .isEqualTo(expected);
    }
}