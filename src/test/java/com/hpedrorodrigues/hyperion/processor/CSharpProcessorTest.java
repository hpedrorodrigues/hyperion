package com.hpedrorodrigues.hyperion.processor;

import com.hpedrorodrigues.hyperion.base.AbstractTest;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class CSharpProcessorTest extends AbstractTest {

    private CSharpProcessor processor;

    @Before
    public void setUp() {
        this.processor = new CSharpProcessor();
    }

    @Test
    public void processFileName() {
        final String expected = "Test.cs";

        Assertions
                .assertThat(processor.getFileName("test"))
                .isEqualTo(expected);
    }

    @Test
    public void processPackageName() {
        final String expected = "namespace test {\n\n";

        Assertions
                .assertThat(processor.getNamespace("test"))
                .isEqualTo(expected);
    }

    @Test
    public void processStartClass() {
        final String expected = "\tpublic class Test {";

        Assertions
                .assertThat(processor.getStartClass("test"))
                .isEqualTo(expected);
    }

    @Test
    public void processType() {
        final String expected = "\n\n\t\tpublic Int test { get; set; }";

        Assertions
                .assertThat(processor.getType("test", "Int"))
                .isEqualTo(expected);
    }

    @Test
    public void processListType() {
        final String expected = "\n\n\t\tpublic List<String> test { get; set; }";

        Assertions
                .assertThat(processor.getListType("test", "String"))
                .isEqualTo(expected);
    }

    @Test
    public void processListTypeName() {
        final String expected = "List<string>";

        Assertions
                .assertThat(processor.getListTypeName("string"))
                .isEqualTo(expected);
    }

    @Test
    public void processListSetter() {
        Assertions
                .assertThat(processor.getListSetter("test", "Double"))
                .isEmpty();
    }

    @Test
    public void processListGetter() {
        Assertions
                .assertThat(processor.getListGetter("test", "Boolean"))
                .isEmpty();
    }

    @Test
    public void processSetter() {
        Assertions
                .assertThat(processor.getSetter("test", "Float"))
                .isEmpty();
    }

    @Test
    public void processGetter() {
        Assertions
                .assertThat(processor.getGetter("test", "Integer"))
                .isEmpty();
    }
}