package com.hpedrorodrigues.hyperion.processor;

import com.hpedrorodrigues.hyperion.base.AbstractTest;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class PHPProcessorTest extends AbstractTest {

    private PHPProcessor processor;

    @Before
    public void setUp() {
        this.processor = new PHPProcessor();
    }

    @Test
    public void processFileName() {
        final String expected = "Test.php";

        Assertions
                .assertThat(processor.getFileName("test"))
                .isEqualTo(expected);
    }

    @Test
    public void processPackageName() {
        Assertions
                .assertThat(processor.getNamespace("test"))
                .isEmpty();
    }

    @Test
    public void processStartClass() {
        final String expected = "class Test {\n";

        Assertions
                .assertThat(processor.getStartClass("test"))
                .isEqualTo(expected);
    }

    @Test
    public void processType() {
        final String expected = "\n\tprivate $test;  // Integer";

        Assertions
                .assertThat(processor.getType("test", "Integer"))
                .isEqualTo(expected);
    }

    @Test
    public void processListType() {
        final String expected = "\n\tprivate $test;  // String List";

        Assertions
                .assertThat(processor.getListType("test", "String"))
                .isEqualTo(expected);
    }

    @Test
    public void processListTypeName() {
        final String expected = "String List";

        Assertions
                .assertThat(processor.getListTypeName("String"))
                .isEqualTo(expected);
    }

    @Test
    public void processListSetter() {
        final String expected = "\n\tpublic function setTest($test) {\n\t\t$this->test = $test;\n\t}\n";

        Assertions
                .assertThat(processor.getListSetter("test", "Double"))
                .isEqualTo(expected);
    }

    @Test
    public void processListGetter() {
        final String expected = "\n\tpublic function getTest() {\n\t\treturn $this->test;\n\t}\n";

        Assertions
                .assertThat(processor.getListGetter("test", "Boolean"))
                .isEqualTo(expected);
    }

    @Test
    public void processSetter() {
        final String expected = "\n\tpublic function setTest($test) {\n\t\t$this->test = $test;\n\t}\n";

        Assertions
                .assertThat(processor.getSetter("test", "Float"))
                .isEqualTo(expected);
    }

    @Test
    public void processGetter() {
        final String expected = "\n\tpublic function getTest() {\n\t\treturn $this->test;\n\t}\n";

        Assertions
                .assertThat(processor.getGetter("test", "Integer"))
                .isEqualTo(expected);
    }
}