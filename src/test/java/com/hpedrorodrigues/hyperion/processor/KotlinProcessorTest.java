package com.hpedrorodrigues.hyperion.processor;

import com.hpedrorodrigues.hyperion.base.AbstractTest;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class KotlinProcessorTest extends AbstractTest {

    private KotlinProcessor processor;

    @Before
    public void setUp() {
        this.processor = new KotlinProcessor();
    }

    @Test
    public void processFileName() {
        final String expected = "Test.kt";

        Assertions
                .assertThat(processor.getFileName("test"))
                .isEqualTo(expected);
    }

    @Test
    public void processPackageName() {
        final String expected = "package test\n\n";

        Assertions
                .assertThat(processor.getNamespace("test"))
                .isEqualTo(expected);
    }

    @Test
    public void processStartClass() {
        final String expected = "class Test {";

        Assertions
                .assertThat(processor.getStartClass("test"))
                .isEqualTo(expected);
    }

    @Test
    public void processType() {
        final String expected = "\n\n\tvar test: Int = 0";

        Assertions
                .assertThat(processor.getType("test", "Int"))
                .isEqualTo(expected);
    }

    @Test
    public void processListType() {
        final String expected = "\n\n\tvar test: List<String> = null";

        Assertions
                .assertThat(processor.getListType("test", "String"))
                .isEqualTo(expected);
    }

    @Test
    public void processListTypeName() {
        final String expected = "List<String>";

        Assertions
                .assertThat(processor.getListTypeName("String"))
                .isEqualTo(expected);
    }

    @Test
    public void processListSetter() {
        Assertions
                .assertThat(processor.getListSetter("test", "Double"))
                .isEmpty();
    }

    @Test
    public void processListGetter() {
        Assertions
                .assertThat(processor.getListGetter("test", "Boolean"))
                .isEmpty();
    }

    @Test
    public void processSetter() {
        Assertions
                .assertThat(processor.getSetter("test", "Float"))
                .isEmpty();
    }

    @Test
    public void processGetter() {
        Assertions
                .assertThat(processor.getGetter("test", "Integer"))
                .isEmpty();
    }
}