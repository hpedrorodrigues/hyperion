package com.hpedrorodrigues.hyperion.processor;

import com.hpedrorodrigues.hyperion.base.AbstractTest;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class JavaProcessorTest extends AbstractTest {

    private JavaProcessor processor;

    @Before
    public void setUp() {
        this.processor = new JavaProcessor();
    }

    @Test
    public void processFileName() {
        final String expected = "Test.java";

        Assertions
                .assertThat(processor.getFileName("test"))
                .isEqualTo(expected);
    }

    @Test
    public void processPackageName() {
        final String expected = "package test;\n\n";

        Assertions
                .assertThat(processor.getNamespace("test"))
                .isEqualTo(expected);
    }

    @Test
    public void processStartClass() {
        final String expected = "public class Test {\n";

        Assertions
                .assertThat(processor.getStartClass("test"))
                .isEqualTo(expected);
    }

    @Test
    public void processType() {
        final String expected = "\n\tprivate Integer test;";

        Assertions
                .assertThat(processor.getType("test", "Integer"))
                .isEqualTo(expected);
    }

    @Test
    public void processListType() {
        final String expected = "\n\tprivate List<String> test;";

        Assertions
                .assertThat(processor.getListType("test", "String"))
                .isEqualTo(expected);
    }

    @Test
    public void processListTypeName() {
        final String expected = "List<String>";

        Assertions
                .assertThat(processor.getListTypeName("String"))
                .isEqualTo(expected);
    }

    @Test
    public void processListSetter() {
        final String expected = "\n\tpublic void setTest(List<Double> test) {\n\t\tthis.test = test;\n\t}\n";

        Assertions
                .assertThat(processor.getListSetter("test", "Double"))
                .isEqualTo(expected);
    }

    @Test
    public void processListGetter() {
        final String expected = "\n\tpublic List<Boolean> getTest() {\n\t\treturn this.test;\n\t}\n";

        Assertions
                .assertThat(processor.getListGetter("test", "Boolean"))
                .isEqualTo(expected);
    }

    @Test
    public void processSetter() {
        final String expected = "\n\tpublic void setTest(Float test) {\n\t\tthis.test = test;\n\t}\n";

        Assertions
                .assertThat(processor.getSetter("test", "Float"))
                .isEqualTo(expected);
    }

    @Test
    public void processGetter() {
        final String expected = "\n\tpublic Integer getTest() {\n\t\treturn this.test;\n\t}\n";

        Assertions
                .assertThat(processor.getGetter("test", "Integer"))
                .isEqualTo(expected);
    }
}