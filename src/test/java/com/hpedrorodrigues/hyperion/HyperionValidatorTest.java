package com.hpedrorodrigues.hyperion;

import com.hpedrorodrigues.hyperion.base.AbstractTest;
import com.hpedrorodrigues.hyperion.configuration.Metadata;
import com.hpedrorodrigues.hyperion.model.enumeration.Language;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class HyperionValidatorTest extends AbstractTest {

    private HyperionValidator validator;

    @Before
    public void setUp() {
        validator = new HyperionValidator();
    }

    @Test(expected = NullPointerException.class)
    public void validateMetadataObject() throws Exception {
        validator.validate(null);
    }

    @Test(expected = NullPointerException.class)
    public void validateLanguageField() throws Exception {
        validator.validate(new Metadata(Language.JAVA, null));
    }

    @Test(expected = NullPointerException.class)
    public void validateJSONField() throws Exception {
        final Metadata metadata = new Metadata();
        metadata.setJson("{\"a\": 1}");

        validator.validate(metadata);
    }

    @Test
    public void validateAValidMetadata() throws Exception {
        try {
            validator.validate(new Metadata(Language.JAVA, "{\"a\": 1}"));
        } catch (final NullPointerException e) {
            Assertions.fail("Validator throws exception to a valid metadata!");
        }
    }
}