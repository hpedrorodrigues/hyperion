package com.hpedrorodrigues.hyperion;

import com.hpedrorodrigues.hyperion.base.AbstractTest;
import com.hpedrorodrigues.hyperion.configuration.Metadata;
import com.hpedrorodrigues.hyperion.model.PlainEntity;
import com.hpedrorodrigues.hyperion.model.enumeration.Language;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.List;

@RunWith(JUnit4.class)
public class HyperionTest extends AbstractTest {

    @Test
    public void parseSimpleObjectToJava() {
        final String expectedContent = "package " + Metadata.DEFAULT_PACKAGE + ";\n\n" +
                "public class " + Metadata.DEFAULT_ENTITY + " {\n\n\t" +
                "private String a;\n\n\t" +
                "public String getA() {\n\t\treturn this.a;\n\t}\n\n\t" +
                "public void setA(String a) {\n\t\tthis.a = a;\n\t}\n" +
                "\n}";

        final Metadata metadata = new Metadata(Language.JAVA, "{\"a\": \"a\"}");
        final List<PlainEntity> entities = Hyperion.parse(metadata);
        Assertions.assertThat(entities).isNotEmpty();

        final PlainEntity actualEntity = entities.get(0);
        Assertions.assertThat(actualEntity.getName())
                .isEqualTo(Metadata.DEFAULT_ENTITY + ".java");
        Assertions.assertThat(actualEntity.getContent()).isEqualTo(expectedContent);
    }

    @Test
    public void parseSimpleObjectSpecifyingRootEntityNameToJava() {
        final String expectedContent = "package " + Metadata.DEFAULT_PACKAGE + ";\n\n" +
                "public class Test {\n\n\t" +
                "private String a;\n\n\t" +
                "public String getA() {\n\t\treturn this.a;\n\t}\n\n\t" +
                "public void setA(String a) {\n\t\tthis.a = a;\n\t}\n" +
                "\n}";

        final Metadata metadata = new Metadata();

        metadata.setLanguage(Language.JAVA);
        metadata.setRootEntityName("test");
        metadata.setJson("{\"a\": \"a\"}");

        final List<PlainEntity> entities = Hyperion.parse(metadata);
        Assertions.assertThat(entities).isNotEmpty();

        final PlainEntity actualEntity = entities.get(0);
        Assertions.assertThat(actualEntity.getName()).isEqualTo("Test.java");
        Assertions.assertThat(actualEntity.getContent()).isEqualTo(expectedContent);
    }

    @Test
    public void parseSimpleObjectSpecifyingOnlyLanguageAndJSON() {
        final String expectedContent = "package " + Metadata.DEFAULT_PACKAGE + ";\n\n" +
                "public class " + Metadata.DEFAULT_ENTITY + " {\n\n\t" +
                "private String a;\n\n\t" +
                "public String getA() {\n\t\treturn this.a;\n\t}\n\n\t" +
                "public void setA(String a) {\n\t\tthis.a = a;\n\t}\n" +
                "\n}";

        final List<PlainEntity> entities = Hyperion.parse(Language.JAVA, "{\"a\": \"a\"}");
        Assertions.assertThat(entities).isNotEmpty();

        final PlainEntity actualEntity = entities.get(0);
        Assertions.assertThat(actualEntity.getName()).isEqualTo(Metadata.DEFAULT_ENTITY + ".java");
        Assertions.assertThat(actualEntity.getContent()).isEqualTo(expectedContent);
    }

    @Test
    public void parseEmptyObject() {
        final String expectedContent = "package " + Metadata.DEFAULT_PACKAGE + ";\n\n" +
                "public class Test {\n\n}";

        final Metadata metadata = new Metadata();

        metadata.setLanguage(Language.JAVA);
        metadata.setRootEntityName("test");
        metadata.setJson("{}");

        final List<PlainEntity> entities = Hyperion.parse(metadata);
        Assertions.assertThat(entities).isNotEmpty();

        final PlainEntity actualEntity = entities.get(0);
        Assertions.assertThat(actualEntity.getName()).isEqualTo("Test.java");
        Assertions.assertThat(actualEntity.getContent()).isEqualTo(expectedContent);
    }
}